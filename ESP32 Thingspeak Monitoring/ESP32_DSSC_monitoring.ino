#include <WiFi.h>
#include <HTTPClient.h>
#include <esp_sleep.h>

#define uS_TO_S_FACTOR 1000000ULL  // 將微秒換算為秒 
#define TIME_TO_SLEEP  1800        // ESP32深度睡眠間隔（秒） 

const int potPin = 34; //讀入太陽能輸出腳位
int potValue = 0;

const char* ssid = "SSID"; //請依自己的Thingspeak帳號設定
const char* password = "密碼"; //請依自己的Thingspeak帳號設定

const char* serverName = "http://api.thingspeak.com/update";
String apiKey = "apiKey"; //請依自己的Thingspeak帳號設定

unsigned long lastTime = 0;

void setup() {
  Serial.begin(115200);

  // 設定深度睡眠來節省耗電量
  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
}

void loop() {
  // 讀取太陽能腳位
  potValue = analogRead(potPin);

  // 連結 WiFi
  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());

  // 上傳資料到 ThingSpeak
  WiFiClient client;
  HTTPClient http;
  http.begin(client, serverName);
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");
  String httpRequestData = "api_key=" + apiKey + "&field1=" + String(potValue);
  int httpResponseCode = http.POST(httpRequestData);
  Serial.print("HTTP Response code: ");
  Serial.println(httpResponseCode);
  http.end();

  // 解除連線 WiFi
  WiFi.disconnect(true);

  Serial.println("Going to sleep now");
  Serial.flush(); 
  esp_deep_sleep_start();
}
